
  // type definitions for Cypress object "cy"
  /// <reference types="cypress" />
  import HomePage from '../../support/pageObjects/HomePage' 
  import InspirationPage from '../../support/pageObjects/InspirationPage'
  import Global from '../../support/pageObjects/Global'
  
  describe('Verifying Spaces/Categories Feature', function() 

  {
    const homePage=new HomePage();
    const inspirationPage=new InspirationPage();
    const global= new Global();
   
    this.beforeEach(function() {
      // runs once before all tests in the block
      cy.fixture('inspiration').then(function(data)
      {
       this.data=data
      })
    })
    it('Visits the inspiration page check space UI and descrption displayed in header for space', function()
    {
     cy.visit("https://" + Cypress.env('environment') +"homluv.com");
     global.getHeader_explore().click()
     cy.request('/').its('body')
     inspirationPage.getHeaderSpace().should('have.text',this.data.Space_allcategories)
     inspirationPage.getHeaderSpace().click()
     inspirationPage.getspaceKitchen().should('be.visible')
     inspirationPage.getspaceBathroom().should('be.visible')
     inspirationPage.getspaceBedroom().should('be.visible')
     inspirationPage.getspaceLiving().should('be.visible')
     inspirationPage.getspaceExterior().should('be.visible')
     }) 
     it('Visits the inspiration page and check the selection of spaces and text displayed for the same', function()
    {
    inspirationPage.getspaceKitchen().invoke("removeAttr", "target").click()
    inspirationPage.getHeaderSpace().should('have.text',this.data.Space_kitchen)
    inspirationPage.getspaceBedroom().click()
    inspirationPage.getHeaderSpace().should('have.text',this.data.Space_morethan1category)
     }) 
     
})
