  // type definitions for Cypress object "cy"
  /// <reference types="cypress" />
  import HomePage from '../../support/pageObjects/HomePage' 
  import InspirationPage from '../../support/pageObjects/InspirationPage'
  import Global from '../../support/pageObjects/Global'
  
  describe('Verifying SignIn Feature', function() 

  {
    const homePage=new HomePage();
    const inspirationPage=new InspirationPage();
    const global= new Global();
   
    this.beforeEach(function() {
      // runs once before all tests in the block
      cy.fixture('global').then(function(data)
      {
       this.data=data
      })
    })
    it('Visits the homluv home page and validating the texts of signIn Modal heading and its subheading', function()
    {
     cy.visit("https://" + Cypress.env('environment') +"homluv.com");
     global.getSignInBtn().click();
     global.getSignInModal_Heading().should('have.text',this.data.SignIn_Heading)
     global.getSignInModal_LeftHeading().should('have.text',this.data.SignIn_LeftHeading)
     global.getSignInModal_LeftPara().should('have.text',this.data.SignIn_Para)
     }) 
     it('Visits the homluv home page and signIn with valid data', function()
    {
     cy.visit("https://" + Cypress.env('environment') +"homluv.com");
     global.getSignInBtn().click();
     global.getSignInModal_EmailAddress().type(this.data.SignIn_ValidEmail)
     global.getSignInModal_Password().type(this.data.SignIn_ValidPWD)
     global.getSignInModal_SignInBtn().click();
     cy.wait(5000);
     global.getHeader_ProfileIcon().should('be.visible')
     }) 
     
})