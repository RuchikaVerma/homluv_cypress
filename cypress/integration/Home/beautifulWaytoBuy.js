// type definitions for Cypress object "cy"
/// <reference types="cypress" />
import HomePage from "../../support/pageObjects/HomePage";
import InspirationPage from "../../support/pageObjects/InspirationPage";

describe("Verifying Beautifullway section", function() {
  const homePage = new HomePage();
  const inspirationPage = new InspirationPage();

  this.beforeEach(function() {
    // runs once before all tests in the block
    cy.fixture("home").then(function(data) {
      this.data = data;
    });
  });
  it("Visits the homluv home page and validating the texts of heading and its subheading", function() {
    cy.visit("https://" + Cypress.env("environment") + "homluv.com");
    cy.scrollTo(0, 1800);
    homePage
      .getBeautifullWayTitle()
      .should("have.text", this.data.BeautifullwayTitle);
    homePage
      .getBeautifullWayFirstPara()
      .should("have.text", this.data.BeautifullwayfirstPara);
    homePage.getBeautifullWayReadMoreText().click();
    homePage
      .getBeautifullWaySecondPara()
      .should("have.text", this.data.BeautifullwaySecondPara);
    homePage
      .getBeautifullWayThirdPara()
      .should("have.text", this.data.BeautifullwayThirdPara);
    homePage
      .getBeautifullWayFourthPara()
      .should("have.text", this.data.BeautifullwayFourthPara);
    homePage
      .getBeautifullWayFifthPara()
      .should("have.text", this.data.BeautifullwayFifthPara);
    homePage.getBeautifullwayCollapseViewbtn().click();
    homePage
      .getBeautifullWayReadMoreText()
      .should("have.text", "Continue Reading");
    cy.scrollTo(0, 1800);
    homePage.getBeautifullWayImg().should("be.visible");
    homePage.getBeautifullWayImg().should("have.attr", "alt");
  });
});
