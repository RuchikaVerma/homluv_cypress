// type definitions for Cypress object "cy"
/// <reference types="cypress" />
import HomePage from "../../support/pageObjects/HomePage";
import InspirationPage from "../../support/pageObjects/InspirationPage";

describe("Verifying location search", function() {
  const homePage = new HomePage();
  const inspirationPage = new InspirationPage();

  this.beforeEach(function() {
    // runs once before all tests in the block
    cy.fixture("home").then(function(data) {
      this.data = data;
    });
  });
  it("Visits the homluv home page and making a search and checking the result in dropdown is as per search or not", function() {
    cy.visit("https://" + Cypress.env("environment") + "homluv.com");
    homePage.getSearchLocation().type(this.data.location);
    cy.get(".is-visible > ul").each((item, index) => {
      cy.wrap(item).should("contain.text", this.data.location);
      cy.reload();
    });
  });

  it("Making a search using dropdown feature", function() {
    homePage.getSearchLocation().type(this.data.location);
    cy.wait(2000);
    homePage
      .getdropdownlist()
      .contains(this.data.Entered_search)
      .click();
    homePage.getSearchSubmitbtn().click();
    cy.url().should("include", "inspiration");
    cy.reload();
    inspirationPage
      .getHeaderSearchedLocation()
      .should("have.text", this.data.Searched_Loc);
    homePage.getHomeLogo().click();
  });

  it("Validating the error message on wrong search", function() {
    homePage.getSearchLocation().type(this.data.wrong_search);
    homePage.getSearchSubmitbtn().click();
    homePage
      .getSearchNoResult()
      .should("have.text", this.data.search_error_msg);
  });
});
