// type definitions for Cypress object "cy"
/// <reference types="cypress" />
import HomePage from "../../support/pageObjects/HomePage";
import InspirationPage from "../../support/pageObjects/InspirationPage";

describe("Shop What Matters to You(categories) section verification", function() {
  const homePage = new HomePage();
  const inspirationPage = new InspirationPage();
  this.beforeEach(function() {
    // runs once before all tests in the block
    cy.fixture("home").then(function(data) {
      this.data = data;
    });
  });
  it("Visits the homluv home page and validating the static data displayed of Shop What Matters to You", function() {
    cy.visit("https://" + Cypress.env("environment") + "homluv.com");
    cy.scrollTo(0, 1200);
    homePage
      .getCategory_heading()
      .should("have.text", this.data.category_heading);
    homePage
      .getCategory_subheading()
      .should("have.text", this.data.category_subheading);
  });
  it("validating the Categories displayed in Shop What Matters to You section", function() {
    homePage.getKitchensCategory().should("have.text", "KITCHENS");
    homePage.getBedroomsCategory().should("have.text", "BEDROOMS");
    homePage.getLivingAreasCategory().should("have.text", "LIVING AREAS");
    homePage.getBathsCategory().should("have.text", "BATHS");
    homePage.getExteriorsCategory().should("have.text", "EXTERIORS");
  });
  it("validating the Kitchen Categories displayed in Shop What Matters to You section", function() {
    homePage.getKitchensCategory().click();
    homePage.getSearchModal().should("be.visible");
    homePage.getSearchModal().invoke("show");
    homePage.getSearchModal_searchtextbox().type(this.data.location);
    cy.wait(2000);
    homePage
      .getSearchModal_dropdown()
      .contains(this.data.Entered_search)
      .click();
    homePage.getSearchModal_SearchBtn().should("be.visible");
    homePage.getSearchModal_SearchBtn().click();
    cy.url().should("include", "inspiration");
    cy.reload();
    inspirationPage
      .getHeaderSearchedLocation()
      .should("have.text", this.data.Searched_Loc);
    homePage.getHomeLogo().click();
  });
});
