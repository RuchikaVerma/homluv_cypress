// type definitions for Cypress object "cy"
/// <reference types="cypress" />
import HomePage from "../../support/pageObjects/HomePage";
import InspirationPage from "../../support/pageObjects/InspirationPage";
import Global from "../../support/pageObjects/Global";

describe("Verifying YourDreamHomeAwaits section", function() {
  const homePage = new HomePage();
  const inspirationPage = new InspirationPage();
  const global = new Global();

  this.beforeEach(function() {
    // runs once before all tests in the block
    cy.fixture("home").then(function(data) {
      this.data = data;
    });
  });
  it("Verify the UI elements including heading subheading of Your Dream Home Awaits", function() {
    cy.visit("https://" + Cypress.env("environment") + "homluv.com");
    cy.scrollTo(0, 2500);
    cy.wait(2000);
    homePage
      .getWhyNotStartNewHeading()
      .should("have.text", this.data.YourDreamHomeAwaits_Heading);
    homePage
      .getWhyNotStartNewPara()
      .should("have.text", this.data.YourDreamHomeAwaits_SubHeading);
    homePage.getWhyNotStartSection().should("be.visible");
    homePage.getWhyNotStartNewPrivacyText().should("be.visible");
  });
  it("Verify the Your Dream Home Awaits Functionality by name, email validation", function() {
    homePage.getWhyNotStartNewSignupButton().click();
    homePage
      .getWhyNotStartNameAlert()
      .should("have.text", this.data.YourDreamHomeAwaits_NameAlertmsg);
    homePage
      .getWhyNotStartNewEmailAlert()
      .should("have.text", this.data.YourDreamHomeAwaits_EmailAlertmsg);
    homePage
      .getWhyNotStartCheckboxAlert()
      .should("have.text", this.data.YourDreamHomeAwaits_CheckboxAlertmsg);
  });
  it("Verify the Your Dream Home Awaits Functionality checkbox validation", function() {
    homePage.getWhyNotStartNewName().type(this.data.YourDreamHomeAwaits_Name);
    homePage.getWhyNotStartNewEmail().type(this.data.YourDreamHomeAwaits_Email);
    homePage.getWhyNotStartNewSignupButton().click();
    homePage
      .getWhyNotStartCheckboxAlert()
      .should("have.text", this.data.YourDreamHomeAwaits_CheckboxAlertmsg);
  });
  it("Verify the Your Dream Home Awaits Functionality filled data", function() {
    homePage.getWhyNotStartNewCheckbox().click({ force: true });
    homePage.getWhyNotStartNewSignupButton().click();
    global.getSignupModal().should("be.visible");
    global.getSignupModal().invoke("removeAttr", "target");
    cy.wait(2000);
    cy.get(".form-title.form-group").should(
      "have.text",
      "Save your Loves! Create an Account"
    );

    global
      .getSignup_FirstName()
      .should("have.value", this.data.YourDreamHomeAwaits_Name);
    global
      .getSignup_Email()
      .should("have.value", this.data.YourDreamHomeAwaits_Email);
  });
});
