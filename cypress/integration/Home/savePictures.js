// type definitions for Cypress object "cy"
/// <reference types="cypress" />
import HomePage from "../../support/pageObjects/HomePage";
import InspirationPage from "../../support/pageObjects/InspirationPage";

describe("Verifying Save Picture section", function() {
  const homePage = new HomePage();
  const inspirationPage = new InspirationPage();

  this.beforeEach(function() {
    // runs once before all tests in the block
    cy.fixture("home").then(function(data) {
      this.data = data;
    });
  });
  it("Visits the homluv home page and validating the texts of heading and its subheading", function() {
    cy.visit("https://" + Cypress.env("environment") + "homluv.com");

    homePage
      .getSavePictureHeading()
      .should("have.text", this.data.SavePicture_Heading);
    homePage
      .getSavePictureSubHeading()
      .should("have.text", this.data.SavePicture_SubHeading);
  });
});
