// type definitions for Cypress object "cy"
/// <reference types="cypress" />
import HomePage from "../../support/pageObjects/HomePage";
import InspirationPage from "../../support/pageObjects/InspirationPage";

describe("Verifying builder logo section", function() {
  const homePage = new HomePage();
  const inspirationPage = new InspirationPage();

  this.beforeEach(function() {
    // runs once before all tests in the block
    cy.fixture("home").then(function(data) {
      this.data = data;
    });
  });
  it("Visits the homluv home page and validating the builderlogo title", function() {
    cy.visit("https://" + Cypress.env("environment") + "homluv.com");
    cy.scrollTo(0, 2000);
    homePage
      .getBuilderLogoTitle()
      .should("have.text", this.data.builderlogo_title);
    homePage.getBuilderLogoSection().should("be.visible");

    homePage.getBuilderLogoViewImg().each($el => {
      cy.wrap($el).should("have.attr", "alt");
    });
    homePage.getBuilderLogoViewImg().should("have.length", 10);
  });
});
