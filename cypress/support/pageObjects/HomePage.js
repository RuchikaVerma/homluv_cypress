class HomePage {
  getSearchLocation() {
    return cy.get("input[placeholder='Enter City']"); 
  }
  getdropdownlist() {
    return cy.get(".is-visible > ul");
  }
  getHomeLogo() {
    return cy.get(".logo");
  }
  getSearchSubmitbtn() {
    return cy.get('[value="search your area"]');
  }
  getSearchNoResult() {
    return cy.get(".not-found");
  }
  getCategory_heading() {
    return cy.get(".innovat-title");
  }
  getCategory_subheading() {
    return cy.get(".innovat-subtitle");
  }

  getAllCategory() {
    return cy.get(".innovat-row > .swiper-wrapper");
  }
  getKitchensCategory() {
    return cy.get(":nth-child(1) > .innovat-list-caption > .primary-btn");
  }

  getBedroomsCategory() {
    return cy.get(":nth-child(2) > .innovat-list-caption > .primary-btn");
  }

  getLivingAreasCategory() {
    return cy.get(":nth-child(3) > .innovat-list-caption > .primary-btn");
  }

  getBathsCategory() {
    return cy.get(":nth-child(4) > .innovat-list-caption > .primary-btn");
  }

  getExteriorsCategory() {
    return cy.get(":nth-child(5) > .innovat-list-caption > .primary-btn");
  }
  getSearchModal() {
    return cy.get(".modal-content");
  }
  getSearchModal_searchtextbox() {
    return cy.get("input[placeholder='Search by city name or zip code']"); 
    
  }
  getSearchModal_dropdown() {
    return cy.get(".suggestions-container.is-visible");
  }
  getSearchModal_SearchBtn() {
    return cy.get("div.modal-body > hml-location-auto-complete > div > div.inner-box > input.hl-search-btn.search-btn-text.search-icon");
  }
  getBuilderLogoTitle() {
    return cy.get(".builder-logos-title");
  }
  getBuilderLogoSection() {
    return cy.get(".builder-logos > .swiper-container");
  }
  getBuilderLogoFirstLogo() {
    return cy.get(
      ".builder-logos > .swiper-container > .swiper-wrapper > .swiper-slide-active"
    );
  }
  getBuilderLogoAllimg() {
    return cy.get(".swiper-slide.builder-logos-imgholder");
  }
  getBuilderLogoViewImg() {
    return cy.get(".builder-logos-img.swiper-lazy.swiper-lazy-loaded");
  }

  getSavePictureHeading() {
    return cy.get(".h1-pink");
  }
  getSavePictureSubHeading() {
    return cy.get(".para");
  }
  getBeautifullWayTitle() {
    return cy.get(".title");
  }
  getBeautifullWayFirstPara() {
    return cy.get('.buy-home-contentpara > :nth-child(1)');
  }
  getBeautifullWayReadMoreText() {
    return cy.get(".read-more");
  }
  getBeautifullWaySecondPara() {
    return cy.get(".buy-home-contentpara > :nth-child(2)");
  }
  getBeautifullWayThirdPara() {
    return cy.get(".buy-home-contentpara > :nth-child(3)");
  }
  getBeautifullWayFourthPara() {
    return cy.get(".buy-home-contentpara > :nth-child(4)");
  }
  getBeautifullWayFifthPara() {
    return cy.get(".buy-home-contentpara > :nth-child(5)");
  }
  getBeautifullwayCollapseViewbtn() {
    return cy.get(".read-more");
  }
  getBeautifullWayImg() {
    return cy.get(".buy-home-img > .ng-lazyloaded");
  }
  getWhyNotStartSection() {
    return cy.get(".get-start-container");
  }
  getWhyNotStartNewHeading() {
    return cy.get(".get-start-formtitle");
  }

  getWhyNotStartNewPara() {
    return cy.get(".get-start-formpara");
  }

  getWhyNotStartNewName() {
    return cy.get("#name");
  }

  getWhyNotStartNewEmail() {
    return cy.get("#email");
  }

  getWhyNotStartNewSignupButton() {
    return cy.get(":nth-child(3) > .primary-btn");
  }
  getWhyNotStartNewPrivacyText() {
    return cy.get(".get-start-privacy");
  }
  getWhyNotStartNewPrivacySignIn() {
    return cy.get(".get-start-privacy > span");
  }
  getWhyNotStartNewCheckbox() {
    return cy.get("#signintick-signup");
  }
  getWhyNotStartNameAlert() {
    return cy.get(
      ":nth-child(1) > hml-validate-input.ng-invalid > .error > div"
    );
  }
  getWhyNotStartNewEmailAlert() {
    return cy.get(":nth-child(2) > hml-validate-input.ng-invalid > .error");
  }
  getWhyNotStartCheckboxAlert() {
    return cy.get(".hl-custom-radio-btn > .error");
  }
}
export default HomePage;
