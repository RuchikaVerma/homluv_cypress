class Global {

    //Header
    getSignInBtn()
    {
        return cy.get('.luv-count');
    }
 getHeader_ProfileIcon()
 {
    return cy.get('.head-userimg')
 }
 getHeader_explore()
 {
   return cy.get('.head-nav > :nth-child(1) > a')
 }

    //SigIn Modal
     
    
    getSignInModal_Heading()
    {
        return cy.get('.form-title');
    }
    getSignInModal_LeftHeading()
    {
        return cy.get('.head-title');
    }
    getSignInModal_LeftPara()
    {
        return cy.get('.para > span');
    }
    getSignInModal_EmailAddress()
    {
        return cy.get('form.ng-pristine > :nth-child(2) > .form-control');
    }
    getSignInModal_Password()
    {
        return cy.get(':nth-child(3) > .form-control');
    }
    getSignInModal_ForgotPasswordLink()
    {
        return cy.get('.forgot-pass');
    }
    getSignInModal_SignInBtn()
    {
        return cy.get('form.ng-dirty > :nth-child(5)')
    }
    getSignInModal_ForgotPasswordLink()
    {
        return cy.get('form.ng-pristine > :nth-child(2) > .form-control');
    }


    //Signup Modal
    getSignupModal() {
        return cy.get('.sinup-modal-content.modal-content.fullscreen-modal'); 
    }
    getSignup_FirstName() {
        return cy.get('input[placeholder="First Name"]'); 
    }
    getSignup_LastName(){
        return cy.get('input[formcontrolname="lastName"]');
    }
    getSignup_Email(){
        return cy.get(' input[placeholder="Email Address"]');
    }
    getSignup_Password(){
        return cy.get('input[formcontrolname="password"]');
    }
    getSignup_RePassword(){
        return cy.get('body.home-page.body-scroll-lock:nth-child(2) div.cdk-overlay-container:nth-child(14) div.cdk-global-overlay-wrapper div.cdk-overlay-pane hml-dialog.ng-tns-c35-3.ng-trigger.ng-trigger-dialogContainer.ng-star-inserted:nth-child(2) hml-sign-up.ng-star-inserted div.modal.sinup-modal.modal-animate-in div.sinup-modal-content.modal-content.fullscreen-modal div.sinup-modal-body.modal-body form.modal-body-right.sinup-modal-body-right.ng-pristine.ng-invalid.ng-touched div.form-group-landscape:nth-child(3) div.form-group:nth-child(3) > input.form-control.ng-untouched.ng-pristine.ng-invalid');
    }
   
    getSignup_Zip(){
        return cy.get('body.home-page.body-scroll-lock:nth-child(2) div.cdk-overlay-container:nth-child(14) div.cdk-global-overlay-wrapper div.cdk-overlay-pane hml-dialog.ng-tns-c35-3.ng-trigger.ng-trigger-dialogContainer.ng-star-inserted:nth-child(2) hml-sign-up.ng-star-inserted div.modal.sinup-modal.modal-animate-in div.sinup-modal-content.modal-content.fullscreen-modal div.sinup-modal-body.modal-body form.modal-body-right.sinup-modal-body-right.ng-pristine.ng-invalid.ng-touched div.two-input-box:nth-child(4) div.form-group:nth-child(1) > input.form-control.ng-untouched.ng-pristine.ng-invalid');
    }
    getSignup_outsideUsCheckbox(){
        return cy.get('input[formcontrolname="isOutsideUS"]');
    }   
    
}
    export default Global