class InspirationPage {
    getHeaderSearchedLocation() {
        return cy.get('span.location > span');
    }
    
    getHomeLogo(){
        return cy.get('.logo');
    }
    //Header part

    getHeaderContainer()
    {
        return cy.get('.hd-container')
    }
   getHeaderCount()
   {
    return cy.get('.heart-count > :nth-child(2)')
   }
   getloader()
   {
    return cy.get('.overlay > .loader > .loader-dot12')
   }
   getGlobaHeaderLoc()
   {
    return cy.get('.location-name')
   }
   getAllSpaces()
   {
    return cy.get('//span[contains(text(),\'All Spaces\')]')
   }
   getSortBy()
   {
    return cy.get('.sorting-item-title')
   }
   getLeftSideBarSection()
   {
    return cy.get('hml-left-sidebar')
   }
   getMapCollapse()
   {
    return cy.get('map-collapse')
   }
   getLeftCollapseHomesSection()
   {
    return cy.get('hml-left-sidebar-homes > div.top-bottom-space.opacity')
   }

   getLeftCollapseLovesSection()
   {
    return cy.get('hml-left-sidebar-images > div.top-bottom-space.opacity')
   }

   getCollapseShare()
   {
    return cy.get('.coll-share-icon.ng-star-inserted')
   }
   getLeftCollapseHomeTitle()
   {
    return cy.get(' hml-left-sidebar-homes > div.top-bottom-space.opacity > h5.collapse-title > span')
   }
   getLeftCollapseLoveTitle()
   {
    return cy.get('hml-left-sidebar-images > div.top-bottom-space.opacity')
   }
   getLeftCollapseFeatureTitle()
   {
    return cy.get(' div.binfo-feature-btn > span:first-child')
   }
//    getLeftCollapseLoveTitle()
//    {
//     return cy.get('hml-left-sidebar-images > div.top-bottom-space.opacity')
//    }
   getLefCollapseSeeAllTitle()
   {
    return cy.get('.see-all.collapse-title')
   }
   
   

  


   


    //Space elements(categories)
    getHeaderSpace()
    {
        return cy.get('.active > .selected-item')
    }
    getspaceKitchen()

{
    return cy.get(':nth-child(1) > .cat-list-item-name')
}

getspaceBedroom()
{

    return cy.get(':nth-child(2) > .cat-list-item-name')
}
getspaceBathroom()
{

    return cy.get(':nth-child(3) > .cat-list-item-name')
}
getspaceLiving()
{

    return cy.get(':nth-child(4) > .cat-list-item-name')
}
getspaceExterior()
{

    return cy.get(':nth-child(5) > .cat-list-item-name')
}
   

//Grid
getLuvIconImage()
{

return cy.get('.luv-icon')
}
getImageCaption()
{
    return cy.get('.caption-title')
}

getToastmsg()
{
    return cy.get('hml-load-toast-notifications')
}


}

    export default InspirationPage